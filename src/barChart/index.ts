import { BaseChart } from "../common/BaseChart";
import { GraphicData, unionBy } from "../utils/utilities";
import { AxisConfig, AxisTypes } from "../common/AxisConfig";
import { select } from 'd3-selection';
import { timeFormat } from 'd3-time-format';
import { scaleLinear, scaleBand } from 'd3-scale';
import { axisLeft, axisBottom } from 'd3-axis';
import { max, min } from 'd3-array';
import { easeLinear } from 'd3-ease';
import "d3-transition";

export default class clusteredBarChartd3 extends BaseChart {
	private axisBottomContainer: any;
	private axisBottom: AxisConfig = new AxisConfig(AxisTypes.bottom, "Date");
	private axisLeftContainer: any;
	private axisLeft: AxisConfig = new AxisConfig(AxisTypes.left);
	private data = new Array();
	private dates = new Array();
	private entitiesTarget: any;
	private id: number = 0;
	private parseTime = timeFormat("%b-%Y");
	private scaleBand: any;
	private linesContainer: any;
	private linesCeroContainer: any;
	private valueStatus: string = "status";
	constructor(selection: any) {
		super();
		this.axisBottom.tickPadding = 15;
		this.axisLeft.showLineTicks = false;
		this.axisLeft.ticks = 10;
		this.margin.top = 20;
		this.target = select(selection);
		this.target = this.target.append("div").style("position", "relative");
		if (this.target._groups.length == 0) {
			throw new Error("target not found");
		} else {
			this.resize();
		}
	}


	public clear(): void {
		this.target.html("");
	}

	private calculateTop(data: any, date: Date): any {
		var allDateFound = new Array();
		for (let d of data) {
			if (date.toDateString() == d[this.axisBottom.valueName].toDateString()) {
				allDateFound.push(d);
			}
		}
		var positiveNumbers = new Array();
		var negativeNumbers = new Array();

		for (let d of allDateFound) {
			if (d[this.axisLeft.valueName] > 0) {
				positiveNumbers.push(d[this.axisLeft.valueName]);
			} else if (d[this.axisLeft.valueName] < 0) {
				negativeNumbers.push(d[this.axisLeft.valueName]);
			}
		};
		var position = "center";
		if (negativeNumbers.length == allDateFound.length) {
			position = "bottom";
		} if (positiveNumbers.length == allDateFound.length) {
			position = "top";
		}

		return { 'position': position, 'values': allDateFound };
	}
	
	draw(): void {
		var that = this;
		let entities = this.entitiesTarget
			.selectAll("g.entities")
			.data(this.data).each(function (this: any, data: any, index: number) {
				that.drawBar(index.toString(), data, select(this));
			});

		entities.exit().remove();

		let entityTarget = entities
			.enter()
			.append("g").attr("class", "entities");

		entityTarget.each(function (this: any, data: any, index: number) {
			that.drawBar(index.toString(), data, select(this));
		});
		this.drawSeparatedLine();
		this.drawFaces();
		this.drawToolTip();
		this.drawCeroLine();
	}

	private drawCeroLine(): void {
		this.linesCeroContainer.html("");
		this.linesCeroContainer
			.append("line")
			.attr("stroke-width", "1.5px")
			.attr("x1", 0)
			.attr("y1", this.axisLeft.scale(0))
			.attr("x2", this.width - (this.margin.left + this.margin.right))
			.attr("y2", this.axisLeft.scale(0));
	}

	private drawFaces(): void {
		let that = this;
		if (this.data[0])
			this.axisBottomContainer.selectAll("line").each(function (this: any, date: Date) {
				var foundData = {} as any;
				for (let d of that.data[0].values) {
					if (date.toDateString() == d[that.axisBottom.valueName].toDateString()) {
						foundData = d;
					}
				}
				var status = foundData[that.valueStatus];
				var range = new Array();
				range = that.axisLeft.scale.range();
				if (status) {
					let faceContainer = select(this.parentNode)
						.selectAll("g.face-container")
						.data([status])
						.each(function (this: any, d: any) {
							select(this)
							.attr("transform", "translate(" + [-12.5, -(max(range) + that.margin.top)] + ")")
							.style("display", "block");
							select(this)
								.select("path.face-mouth")
								.attr("d", (d: any) => {
									return that.getMouthStatusPath(d);
								});
							select(this)
								.select("rect")
								.attr("class", (status: any) => status)
						});

					faceContainer.exit().remove();

					var g = faceContainer.enter()
						.append("g")
						.attr("class", "face-container")
						.attr("transform", "translate(" + [-12.5, -(max(range) + that.margin.top)] + ")");

					g
						.append("rect")
						.attr("width", 25)
						.attr("height", 20)
						.attr("class", (status: any) => status)
					g
						.append("path")
						.attr("class", "face-mouth")
						.attr("d", (d: any) => {
							return that.getMouthStatusPath(d);
						});
					g
						.append("circle")
						.attr("class", "circle-eyes")
						.attr("r", "1").attr("cx", "9").attr("cy", "6");
					g
						.append("circle")
						.attr("class", "circle-eyes")
						.attr("r", "1").attr("cx", "16").attr("cy", "6");
				} else {
					select(this.parentNode).selectAll("g.face-container").style("display", "none");
				}
			});
	}

	private drawSeparatedLine() {
		var that = this;
		var lines = this.linesContainer
			.selectAll("line")
			.data(this.dates)
			.each(function (this: any, d: any, index: number) {
				select(this)
					.transition().duration(750)
					.attr("x1", () => {
						if (index !== 0) {
							return that.axisBottom.scale(d) - (that.axisBottom.scale.step() / 2) / that.dates.length;
						} else return 0;
					})
					.attr("y1", () => {
						if (index !== 0) {
							return 0;
						} else return 0;
					})
					.attr("x2", () => {
						if (index !== 0) {
							return that.axisBottom.scale(d) - (that.axisBottom.scale.step() / 2) / that.dates.length;
						} else return 0;
					})
					.attr("y2", () => {
						if (index !== 0) {
							return that.height - that.margin.bottom;
						} else return 0;
					});
			});
		lines.exit().remove();
		lines
			.enter()
			.append("line")
			.attr("stroke-width", "1px")
			.attr("x1", (d: any, i: number) => {
				if (i !== 0) {
					return that.axisBottom.scale(d) - (that.axisBottom.scale.step() / 2) / that.dates.length;
				} else return 0;
			})
			.attr("y1", (d: any, i: number) => {
				if (i !== 0) {
					return 0;
				} else return 0;
			})
			.attr("x2", (d: any, i: number) => {
				if (i !== 0) {
					return that.axisBottom.scale(d) - (that.axisBottom.scale.step() / 2) / this.dates.length;
				} else return 0;
			})
			.attr("y2", (d: any, i: number) => {
				if (i !== 0) {
					return that.height - that.margin.bottom;
				} else return 0;
			});
	}

	private drawToolTip(): void {
		var that = this;
		//show after renderizer all graphic
		this.target.select(".hover-container").transition().delay(1000).style("display", "block");
		var splitWidth = (this.width - (this.margin.right)) / this.dates.length;
		this.hoverContainer.html("");
		this.hoverContainer
			.selectAll("div")
			.data(this.dates)
			.enter()
			.append("div")
			.style("position", "absolute")
			.style("left", (d: any, i: number) => {
				return `${that.axisBottom.scale(d) - (that.axisBottom.scale.step() / 2) / that.dates.length}px`;
			})
			.style("top", "0px")
			.style("width", (d: any, i: number) => {
				return `${splitWidth}px`;
			})
			.style("height", this.height - (this.margin.bottom + this.margin.top) + "px")
			.on("mouseover", function (this: any, date: Date) {
				var allData = unionBy(that.data.map((data: any) => { return data.values; }));
				var foundData = that.calculateTop(allData, date);
				that.tooltipContainer.html("");
				if (typeof that.tooltipCallback === "function") {
					that.tooltipCallback.call(undefined, that.tooltipContainer, foundData.values, date);
				} else {
					that.tooltipContainer
						.append("div")
						.html(that.tooltipHtml(foundData.values, date));
				}
				var tooltipWidth = parseFloat(that.tooltipContainer.style("width").split("px")[0]);
				var tooltipHeight = parseFloat(that.tooltipContainer.style("height").split("px")[0]);
				var topLeft = that.margin.left;
				var topRight = that.width;
				//calculate position
				that.tooltipContainer
					.transition()
					.duration(300)
					.style("opacity", "1")
					.ease(easeLinear)
					.style("left", function () {
						var leftPosition = that.axisBottom.scale(date) + (that.axisBottom.scale.bandwidth());//center
						var topLeftTooltip = leftPosition - tooltipWidth;
						var topRightTooltip = leftPosition + tooltipWidth;

						if (foundData.position == "top" || foundData.position == "bottom") {
							if (topLeftTooltip > topLeft && topRightTooltip < topRight) {
								leftPosition = (that.axisBottom.scale(date) + (that.axisBottom.scale.bandwidth() / 2) - (tooltipWidth / 2));
							} else {
								if (topLeftTooltip > topLeft && topRightTooltip > topRight) {
									leftPosition = leftPosition - tooltipWidth;
								} else {
									leftPosition = that.axisBottom.scale(date) + that.axisBottom.scale.bandwidth() / 2;
								}
							}
						} else {//middle
							if ((leftPosition + tooltipWidth) > topRight) {
								leftPosition = that.axisBottom.scale(date) - tooltipWidth;
							} else {
								leftPosition = that.axisBottom.scale(date) + that.axisBottom.scale.bandwidth();
							}
						}
						if (topLeftTooltip < topLeft && topRightTooltip > topRight) {
							leftPosition = 0;
						}
						return leftPosition + "px";
					}).style("top", function () {
						var topPosition = that.axisLeft.scale(0);
						if (foundData.position == "top") {
							topPosition = topPosition + that.margin.top + 5;
						} else if (foundData.position == "bottom") {
							topPosition = topPosition - tooltipHeight;
						}
						var leftPosition = that.axisBottom.scale(date) + that.axisBottom.scale.bandwidth() / 2;//center
						var topLeftTooltip = leftPosition - tooltipWidth;
						var topRightTooltip = leftPosition + tooltipWidth;
						if (topLeftTooltip < topLeft && topRightTooltip > topRight) {
							topPosition = - (tooltipHeight);
						}
						return topPosition + "px";
					});

			})
			.on("mouseleave", (d: any) => {
				this.tooltipContainer
					.style("opacity", "1")
					.transition()
					.duration(300)
					.ease(easeLinear)
					.style("opacity", "0");
			});
	}

	private drawBar(index: string, data: any, target: any): void {
		var that = this;
		var barsTarget = target.selectAll("rect.bar")
			.data(data.values)
			.each(function (this: any) {
				select(this)
					.transition().duration(750)
					.attr("transform", (d: any) => { return "translate(" + [that.axisBottom.scale(d[that.axisBottom.valueName]), 0] + ")"; })
					.attr("fill", function (d: any, i: number) {
						return data.color || that.scaleColor(i.toString());
					})
					.attr("width", that.scaleBand.bandwidth())
					.attr("height", (d: any) => {
						return Math.abs((that.axisLeft.scale(d[that.axisLeft.valueName])) - that.axisLeft.scale(0));
					})
					.attr("x", () => {
						return that.scaleBand(index);
					})
					.attr("y", (d: any, i: number) => {
						return that.axisLeft.scale(Math.max(0, d[that.axisLeft.valueName]));
					});
			});

		barsTarget.exit().remove();

		barsTarget
			.enter()
			.append('rect')
			.attr("class", "bar")
			.attr("transform", (d: any) => { return "translate(" + [that.axisBottom.scale(d[that.axisBottom.valueName]), 0] + ")"; })
			.attr("fill", function (d: any, i: number) {
				return d.color || that.scaleColor(i.toString());
			})
			.attr("width", that.scaleBand.bandwidth())
			.attr("x", () => {
				return that.scaleBand(index);
			})
			.attr("y", (d: any, i: number) => {
				return that.axisLeft.scale(Math.max(0, d[that.axisLeft.valueName]));
			})
			.attr("height", (d: any) => {
				return Math.abs((that.axisLeft.scale(d[that.axisLeft.valueName])) - that.axisLeft.scale(0));
			});
	}

	public empty(): this {
		this.data = new Array();
		this.dates = new Array();
		this.clear();
		this.render();
		var html = this.emptyGraphicContainer
			.append("div")
			.attr("class", "empty-graphic")
			.append("span")
			.html(this.emptyMessage);

		var h = html._groups[0][0].getBoundingClientRect().height;
		var w = html._groups[0][0].getBoundingClientRect().width;

		let top = (this.height / 2) - (h / 2);
		let left = (this.width / 2) - w / 2;
		html.style("top", `${top}px`).style("left", `${left}px`)
		return this;
	}

	private getMouthStatusPath(status: string): string {
		//months path
		var badMouth = "M17.247,13.837c0.011,0.24-0.103,0.361-0.344,0.361c-0.203,0-0.312-0.103-0.324-0.305c-0.063-1.111-0.68-1.666-1.85-1.666h-4.574c-0.635,0-1.086,0.177-1.354,0.53c-0.127,0.163-0.241,0.485-0.343,0.966c-0.077,0.341-0.21,0.506-0.4,0.493c-0.216-0.013-0.318-0.126-0.305-0.342c0.089-1.528,0.883-2.293,2.382-2.293h4.594c1.576,0,2.415,0.759,2.518,2.274h-0.229v-0.02H17.247z",
			regularMouth = "M18.335,13.241c0,0.246-0.2,0.01-0.447,0.01H7.782c-0.247,0-0.447,0.236-0.447-0.01v-0.105 c0-0.248,0.2-0.885,0.447-0.885h10.106c0.247,0,0.447,0.637,0.447,0.885V13.241z",
			goodMouth = "M7.875,11.251c0.133,1,0.575,1.781,1.329,2.535c0.914,0.914,2.013,1.322,3.297,1.322 c1.292,0,2.394-0.383,3.304-1.299c0.749-0.752,1.189-1.559,1.321-2.559H7.875z";
		var path = "";
		switch (status) {
			case "negative":
				path = badMouth;
				break;
			case "regular":
			case "regular2":
				path = regularMouth;
				break;
			case "positive":
				path = goodMouth;
				break;
		}
		return path;
	}

	public initAxisBottom(ifrotate = false, ifUpdate: boolean = false): void {
		let tickSize = this.axisBottom.showLineTicks ? -(this.height - (this.margin.top + this.margin.bottom)) : 0;

		this.axisBottom.containerElement = this.axisBottomContainer.attr("transform", "translate(" + [this.margin.left, (this.height - (this.margin.bottom))] + ")");

		this.axisBottom.setScale((scaleObject: AxisConfig) => {
			scaleObject.scale = scaleBand()
				.domain(this.dates)
				.rangeRound([0, this.width - (this.margin.left + this.margin.right)])
				.paddingInner(0.2);
		});
		this.scaleBand = scaleBand()
			.domain(Object.keys(this.data))
			.rangeRound([0, this.axisBottom.scale.bandwidth()])
			.padding(0.2);
		if (!this.axisBottom.show) return;
		let axis = {} as any;
		axis = axisBottom(this.axisBottom.scale)
			.tickFormat(this.axisBottom.tickFormat === undefined ? (d: any, i) => {
				return this.parseTime(d);
			} : this.axisBottom.tickFormat)
			.tickPadding(this.axisBottom.tickPadding)
			.tickSize(tickSize)
			.ticks(this.dates.length);
		if (ifUpdate) {
			this.axisBottom.containerElement.transition().duration(750).call(axis);
		} else {
			this.axisBottom.containerElement.call(axis);
		}
		if (this.axisBottom.showLineTicks == false)
			this.axisBottom.containerElement.selectAll(".tick line").attr("y2", this.axisBottom.minTickSize);
		if (ifrotate) {
			this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
				select(this).attr("transform", "translate(-25,25)rotate(-60)");
			});
		} else {
			this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
				select(this).attr("transform", "translate(0)rotate(0)");
			});
		}
	}

	public initContainers(): void {
		this.svgContainer = this.target.append("svg")
			.attr("width", this.width)
			.attr("height", this.height);
		this.axisBottomContainer = this.svgContainer
			.append("g")
			.attr("class", "axis-bottom-container");
		this.axisLeftContainer = this.svgContainer
			.append("g")
			.attr("class", "axis-left-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.entitiesTarget = this.svgContainer
			.append("g")
			.attr("class", "entities-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.linesContainer = this.svgContainer
			.append("g")
			.attr("class", "lines-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.linesCeroContainer = this.svgContainer
			.append("g")
			.attr("class", "line-cero-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.tooltipContainer = this.target.append("div")
			.attr("class", "tooltip-container")
			.style("margin-left", this.margin.left + "px")
			.style("width", (this.width - this.margin.left - this.margin.right) + "px")
			.append("div")
			.attr("class", "tooltip-chart")
			.style("opacity", "0");
		this.hoverContainer = this.target.append("div")
			.attr("class", "hover-container")
			.style("left", this.margin.left + "px")
			.style("top", this.margin.top + "px")
			.append("div");
		this.emptyGraphicContainer = this.target.append("div").attr("class", "empty-graphic-container");
	}

	public ifRenderAxisBottom(): [boolean, number] {
		let tickPadding = this.axisBottom.tickPadding;
		let totalTicks = new Array();
		this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
			totalTicks.push(select(this).node().getBBox().width);
		});
		var sum = 0;
		if (totalTicks.length)
			sum = totalTicks.reduce((a, b) => (a + b) + tickPadding);
		var maxValue = max(totalTicks) + 10;
		if (sum > this.width) {
			return [true, maxValue];
		} else {
			return [false, maxValue];
		}
	}

	public initAxisLeft(ifUpdate: boolean = false): void {
		var that = this;
		var min = this.setMin(this.axisLeft.valueName);
		min = this.axisLeft.minValueScaleCero ? 0 : min;
		var max = this.setMax(this.axisLeft.valueName);
		max = this.axisLeft.addMaxValueScalePercentage > 0 ? ((this.axisLeft.addMaxValueScalePercentage * max) / 100) + max : max;
		if (Math.abs(min) > max) {
			max = Math.abs(min);
		}
		this.axisLeft.containerElement = this.axisLeftContainer;
		this.axisLeft.setScale((scaleObject: AxisConfig) => {
			scaleObject.scale = scaleLinear()
				.domain([-(max), max])
				.nice()
				.range([this.height - this.margin.top - this.margin.bottom, 0]);
		});
		if (!this.axisLeft.show) return;
		var axis = {} as any;
		axis = axisLeft(this.axisLeft.scale)
			.tickSize(this.axisLeft.showLineTicks ? -(this.width - (this.margin.left + this.margin.right)) : this.axisLeft.minTickSize)
			.tickPadding(this.axisLeft.tickPadding)
			.tickFormat(function (this: any, d: any, i: number) {
				return that.axisLeft.tickFormat == undefined ? d : that.axisLeft.tickFormat.call(this, d);
			})
			.ticks(this.axisLeft.ticks);
		if (ifUpdate) {
			this.axisLeft.containerElement.transition().duration(750).call(axis);
		} else {
			this.axisLeft.containerElement.call(axis);
		}
		this.axisLeft.containerElement.selectAll("path")
			.attr("stroke", () => { return this.axisLeft.showLineTicks ? "none" : "#e8e8e8" })
			.attr("stroke-width", () => { return this.axisLeft.showLineTicks ? "0" : "2px" });
		if (this.axisLeft.showMinAndMaxTicks) {
			var groupText = this.axisLeft.containerElement.selectAll(".tick text");
			if (groupText._groups.length) {
				groupText.each(function (this: any, d: any, i: number) {
					if (i != 0 && i != groupText._groups[0].length - 1) {
						select(this).text("");
					}
				});
			}
		}
	}

	render(): void {
		this.clear();
		if (this.responsive) {
			this.width = this.target.style("width") === "0px" ? this.width : parseInt(this.target.style("width").replace("px", "")) || this.width;
			this.height = this.target.style("height") === "0px" ? this.height : parseInt(this.target.style("height").replace("px", "")) || this.height;
		}
		this.initContainers();
		//if not show axis bottom changed margin bottom to 10
		if (!this.axisBottom.show)
			this.margin = { ...this.margin, bottom: 15 };
		//check width of axis ticks
		this.initAxisBottom();
		if (this.axisBottom.show) {
			let renderaxis = this.ifRenderAxisBottom();
			if (renderaxis[0]) {
				this.margin = { ...this.margin, bottom: renderaxis[1] };
				this.initAxisBottom(true);
			}
		}
		this.initAxisLeft();
		this.draw();
	}

	public resize(): void {
		window.addEventListener("resize", () => {
			clearTimeout(this.id);
			if (this.data.length > 0)
				this.id = setTimeout(() => {
					this.render();
				}, 1000);
		});
	}

	public setAxisLeft(callback: Function): this {
		callback.call(this, this.axisLeft);
		return this;
	}

	/**
	 * set configuration to axis bottom
	 * @param callback Function(chart, axisBottomConfig), 
	 */
	public setAxisBottom(callback: Function): this {
		callback.call(this, this.axisBottom);
		return this;
	}

	private setMax(value: string): number {
		let maxValue = max(unionBy(this.data.map((v, i) => v.values)), (d: any, i: any): number | undefined => {
			if (d[value] !== undefined) {
				return d[value];
			}
		});
		if (maxValue == undefined) {
			maxValue = 0;
		}
		return maxValue;
	}

	private setMin(value: string): number {
		let minValue = min(unionBy(this.data.map((v, i) => v.values)), (d: any, i: any): number | undefined => {
			if (d[value] !== undefined) {
				return d[value];
			}
		});
		if (minValue == undefined) {
			minValue = 0;
		}
		return minValue;
	}

	public setData(dataset: GraphicData): this {
		if (dataset.data) {
			this.data = dataset.data.map((dd: any, i: number) => {
				dd.color = dd.color === undefined ? this.scaleColor(i.toString()) : dd.color;
				dd.values = dd.values.sort((a: any, b: any) => { return a[this.axisBottom.valueName].getTime() - b[this.axisBottom.valueName].getTime() });
				dd.values = dd.values.map((v: any) => {
					v.color = dd.color;
					return v;
				});
				return dd;
			});
		}
		this.dates = dataset.dates.sort((a: Date, b: Date) => { return a.getTime() - b.getTime() });
		return this;
	}

	public setTooltipCallback(callback: Function): this {
		this.tooltipCallback = callback;
		return this;
	}

	public setResponsive(responsive: boolean): this {
		this.responsive = responsive;
		return this;
	}

	public setStatusValueName(valueStatus: string): this {
		this.valueStatus = valueStatus;
		return this;
	}

	public setEmptyMessage(emptyMessage: string): this {
		this.emptyMessage = emptyMessage
		return this;
	}

	private tooltipHtml(filterValues: any, date: Date): string {
		var ul = `<h6>${this.parseTime(date)}</h6><ul>`;
		filterValues.forEach((d: any, index: number) => {
			var circle = `<div class="_circle_index" style="border-color:${d.color}"></div>`;
			ul += `<li>${circle} ${d.name} : <b>${d[this.axisLeft.valueName]}</b></li>`;
		});
		return `${ul} </ul>`;
	}

	update(): void {
		if (this.responsive) {
			this.width = this.target.style("width") === "0px" ? this.width : parseInt(this.target.style("width").replace("px", "")) || this.width;
			this.height = this.target.style("height") === "0px" ? this.height : parseInt(this.target.style("height").replace("px", "")) || this.height;
		}
		//clear message
		this.target.select(".empty-graphic-container").html("");
		//hide because I don't want to see the tooltip before renderizar the graphic
		this.target.select(".hover-container").style("display", "none");
		//check width of axis ticks
		this.initAxisBottom(false, true);
		if (this.axisBottom.show) {
			let renderaxis = this.ifRenderAxisBottom();
			if (renderaxis[0]) {
				this.margin = { ...this.margin, bottom: renderaxis[1] };
				this.initAxisBottom(true, true);
			}
		}
		this.initAxisLeft(true);
		this.draw();
	}
}

export function run(targetId: string) {
	return new clusteredBarChartd3(targetId);
}