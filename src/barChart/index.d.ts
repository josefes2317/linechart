import { BaseChart } from "../common/BaseChart";
import { GraphicData } from "../utils/utilities";
import "d3-transition";
export default class clusteredBarChartd3 extends BaseChart {
    private axisBottomContainer;
    private axisBottom;
    private axisLeftContainer;
    private axisLeft;
    private data;
    private dates;
    private entitiesTarget;
    private id;
    private parseTime;
    private scaleBand;
    private linesContainer;
    private linesCeroContainer;
    private valueStatus;
    constructor(selection: any);
    clear(): void;
    private calculateTop(data, date);
    draw(): void;
    private drawCeroLine();
    private drawFaces();
    private drawSeparatedLine();
    private drawToolTip();
    private drawBar(index, data, target);
    empty(): this;
    private getMouthStatusPath(status);
    initAxisBottom(ifrotate?: boolean, ifUpdate?: boolean): void;
    initContainers(): void;
    ifRenderAxisBottom(): [boolean, number];
    initAxisLeft(ifUpdate?: boolean): void;
    render(): void;
    resize(): void;
    setAxisLeft(callback: Function): this;
    /**
     * set configuration to axis bottom
     * @param callback Function(chart, axisBottomConfig),
     */
    setAxisBottom(callback: Function): this;
    private setMax(value);
    private setMin(value);
    setData(dataset: GraphicData): this;
    setTooltipCallback(callback: Function): this;
    setResponsive(responsive: boolean): this;
    setStatusValueName(valueStatus: string): this;
    setEmptyMessage(emptyMessage: string): this;
    private tooltipHtml(filterValues, date);
    update(): void;
}
export declare function run(targetId: string): clusteredBarChartd3;
