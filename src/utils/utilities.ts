import { curveCatmullRom, curveMonotoneX, curveBasis, curveLinear } from 'd3-shape';
import { AxisTypes } from '../common/AxisConfig';

/**
 * curves types
 * http://bl.ocks.org/d3indepth/b6d4845973089bc1012dec1674d3aff8
 */
export class CurveLines {
	public curveCatmullRom = curveCatmullRom;
	public curveMonotoneX = curveMonotoneX;
	public curveBasis = curveBasis;
	public curveLinear = curveLinear;
}

export type Data = { axisType?: AxisTypes, color?: string, entityCssClass?: string, values: any[] }

export class GraphicData {
	public data: Array<Data> = [];
	public dates: Array<Date> = [];
}

export function unionBy(value: any[][]) {
	var all = [];
	if (value.length)
		for (var i = 0; i < value.length; ++i) {
			var ff = value[i];
			for (var k = 0; k < ff.length; ++k) {
				all.push(ff[k]);
			}
		}
	return all;
}