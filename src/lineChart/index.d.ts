import { AxisDomain, Axis } from 'd3-axis';
import "d3-transition";
import { GraphicData } from '../utils/utilities';
import { BaseChart } from "../common/BaseChart";
export default class lineChartd3 extends BaseChart {
    private axisLeft;
    private axisRightConfig;
    private axisBottom;
    private areaOpacity;
    private axisLeftContainer;
    private axisRightContainer;
    private axisBottomContainer;
    private entitiesTarget;
    private curveLinesOptions;
    private curveLine;
    private data;
    private dates;
    private ifTwoYscales;
    private id;
    private parseTime;
    private radioCircle;
    private strokeWidthCircle;
    private strokeWidthLine;
    private showCircles;
    private showArea;
    constructor(selection: any);
    private draw(ifTwoYscales);
    private drawCircles(axisType, data, target);
    private drawLine(axisType, data, target);
    private drawArea(axisType, data, target);
    empty(): this;
    getData(): any;
    getTarget(): any;
    private initContainers();
    private renderAxisLeft();
    private renderAxisRight();
    renderAxisBottom(renderAfter: Function): void;
    checkTextWidthOfAxisBottom(): [boolean, number];
    resize(): void;
    /**
     * If it is true create two scale left and right, if the valueName are differents in the AxisConfig and
     * take just two first values of the Data array
     * @param ifTwoYscales by default false
     */
    setIfTwoYscales(ifTwoYscales: boolean): this;
    setShowAreaLine(showArea: boolean): this;
    setAxisBottomDefault(tickSize: number): Axis<AxisDomain>;
    /**
     * d3 curve interpolation - e.m. curveLinear , curveCatmullRom etc.
     * @param callback Function(chart, curveLinesOptions))
     */
    setCurveLine(callback: Function): this;
    /**
     * set configuration to axis left
     * @param callback Function(chart, axisLeftConfig),
     */
    setAxisLeft(callback: Function): this;
    /**
     * set configuration to axis right
     * @param callback Function(chart, axisRightConfig),
     */
    setAxisRight(callback: Function): this;
    /**
     * set configuration to axis bottom
     * @param callback Function(chart, axisBottomConfig),
     */
    setAxisBottom(callback: Function): this;
    /**
     * @param dataset {data:[any],date:[new Dates]}
     */
    setData(dataset: GraphicData): this;
    setTooltipCallback(callback: Function): this;
    setRadioCircle(radioCircle: number): this;
    setStrokeWidthCircle(strokeWidthCircle: number): this;
    setStrokeWidthLine(strokeWidthLine: number): this;
    setWidth(width: number): this;
    setHeight(height: number): this;
    setMargin(margin: {
        top: number;
        bottom: number;
        left: number;
        right: number;
    }): this;
    setResponsive(responsive: boolean): this;
    setAreaOpacity(areaOpacity: string): this;
    private setMax(axis);
    private setMin(axis);
    setShowCircles(showCircles: boolean): this;
    setEmptyMessage(emptyMessage: string): this;
    private showCircle(d);
    private hideCircle(d);
    private renderAxisIfOutRangeofWithContainer();
    private tooltipHtml(filterValues, date);
    render(): void;
    private updateContainer();
    private rejectNull(data, valueName);
}
export declare function run(targetId: string): lineChartd3;
