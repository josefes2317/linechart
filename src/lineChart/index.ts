import { select } from 'd3-selection';
import { timeFormat } from 'd3-time-format';
import { scaleLinear, scaleTime } from 'd3-scale';
import { easeLinear } from 'd3-ease';
import { line, area } from 'd3-shape';
import { axisLeft, axisRight, axisBottom, AxisDomain, Axis } from 'd3-axis';
import { max, min } from 'd3-array';
import "d3-transition";
import { CurveLines, GraphicData, unionBy, Data } from '../utils/utilities';
import { AxisConfig, AxisTypes } from "../common/AxisConfig";
import { BaseChart } from "../common/BaseChart";

export default class lineChartd3 extends BaseChart {
	private axisLeft: AxisConfig = new AxisConfig(AxisTypes.left);
	private axisRightConfig: AxisConfig = new AxisConfig(AxisTypes.right, "Value2");
	private axisBottom: AxisConfig = new AxisConfig(AxisTypes.bottom, "Date");
	private areaOpacity = ".1";
	private axisLeftContainer: any;
	private axisRightContainer: any;
	private axisBottomContainer: any;
	private entitiesTarget: any;
	private curveLinesOptions = new CurveLines();
	private curveLine = this.curveLinesOptions.curveMonotoneX;
	private data = new Array<Data>();
	private dates = new Array();
	private ifTwoYscales: boolean = false;
	private id: number = 0;
	private parseTime = timeFormat("%b-%Y");
	private radioCircle: number = 5;
	private strokeWidthCircle: number = 2;
	private strokeWidthLine: number = 2;
	private showCircles: boolean = false;
	private showArea = false;

	constructor(selection: any) {
		super()
		this.axisBottom.tickPadding = 15;
		this.target = select(selection);
		this.target = this.target.append("div").style("position", "relative");
		if (this.target._groups.length == 0) {
			throw new Error("target not found");
		} else {
			this.initContainers();
			this.resize();
		}
	}

	private draw(ifTwoYscales: boolean): void {
		var that = this;
		var entities = this.entitiesTarget
			.selectAll("g.entities")
			.data(this.data).each(function (this: any, data: Data, index: any) {
				select(this).attr("class", `entities ${data.entityCssClass}`);
				if (ifTwoYscales && data.axisType != undefined && data.axisType == AxisTypes.right) {
					that.drawLine(that.axisRightConfig, data, select(this));
					that.drawArea(that.axisRightConfig, data, select(this));
					that.drawCircles(that.axisRightConfig, data, select(this));
				} else {
					that.drawLine(that.axisLeft, data, select(this));
					that.drawArea(that.axisLeft, data, select(this));
					that.drawCircles(that.axisLeft, data, select(this));
				}
			})

		entities.exit().remove();

		var entityTarget = entities
			.enter()
			.append("g").attr("class", "entities");

		entityTarget.each(function (this: any, data: any, index: any) {
			select(this).attr("class", `entities ${data.entityCssClass}`);
			if (ifTwoYscales && data.axisType != undefined && data.axisType == AxisTypes.right) {
				that.drawLine(that.axisRightConfig, data, select(this));
				that.drawArea(that.axisRightConfig, data, select(this));
				that.drawCircles(that.axisRightConfig, data, select(this));
			} else {
				that.drawLine(that.axisLeft, data, select(this));
				that.drawArea(that.axisLeft, data, select(this));
				that.drawCircles(that.axisLeft, data, select(this));
			}
		});
		//show after renderizer all graphics
		this.target.select(".hover-container").transition().delay(1000).style("display", "block");
		var splitWidth = Math.floor(this.width / this.dates.length);
		this.hoverContainer.html("");
		this.hoverContainer
			.selectAll("div")
			.data(this.dates)
			.enter()
			.append("div")
			.style("position", "absolute")
			.style("left", (d: any) => {
				return `${(this.axisBottom.scale(d) - ((this.width - this.margin.right) / (this.dates.length) / 2))}px`;
			})
			.style("top", "0px")
			.style("width", splitWidth + "px")
			.style("height", this.height - (this.margin.bottom + this.margin.top) + "px")
			.on("mouseover", function (this: any, date: any) {
				let c = that.data.map((data) => { return data.values; })
				var allData = unionBy(c);
				var filterValues = new Array();
				for (var dd of allData) {
					if (date.toDateString() == dd[that.axisBottom.valueName].toDateString()) {
						filterValues.push(dd);
					}
				}
				that.tooltipContainer.html("");
				if (typeof that.tooltipCallback === "function") {
					that.tooltipCallback.call(undefined, that.tooltipContainer, filterValues, date);
				} else {
					that.tooltipContainer
						.append("div")
						.html(that.tooltipHtml(filterValues, date));
				}
				var tooltipWidth = parseFloat(that.tooltipContainer.style("width").split("px")[0]);
				var tooltipHeight = parseFloat(that.tooltipContainer.style("height").split("px")[0]);
				var leftPosition = that.axisBottom.scale(date);
				//calculate position
				that.tooltipContainer
					.transition()
					.duration(300)
					.style("opacity", "1")
					.ease(easeLinear)
					.style("left", function () {
						var max = that.axisBottom.scale.range()[that.axisBottom.scale.range().length - 1] || 0;
						if (leftPosition >= max || (leftPosition + tooltipWidth) > max) {
							leftPosition = leftPosition - (tooltipWidth + 10);
						} else {
							leftPosition += 10;
						}
						return `${(leftPosition)}px`;
					}).style("top", () => {
						var allData = <any>[];
						var topPosition = 0;
						if (this.ifTwoYscales) {

						} else {
							var values = filterValues.map((d: any) => {
								if (that.axisLeft.minValueScaleCero) {
									var value = d[that.axisLeft.valueName];
									if (value < 0) {
										value = 0;
									}
								}
								return that.axisLeft.scale(value);
							});
							var min2 = min(values);
							var max3 = that.axisLeft.scale.range()[0] || 0;
							topPosition = min2;
							if ((tooltipHeight + topPosition) > max3) {
								topPosition = topPosition - tooltipHeight;
							}
						}
						return `${(topPosition)}px`;
					});
				that.showCircle(date);

			})
			.on("mouseleave", (d: any) => {
				this.tooltipContainer
					.style("opacity", "1")
					.transition()
					.duration(300)
					.ease(easeLinear)
					.style("opacity", "0");
				this.hideCircle(d);
			});
	}

	private drawCircles(axisType: AxisConfig, data: Data, target: any): void {
		var that = this;
		var circleTarget = target.selectAll("circle")
			.data(this.rejectNull(data.values, axisType.valueName))
			.each(function (this: any) {
				select(this)
					.transition().duration(750)
					// .attr("fill", "transparent")
					.attr("stroke", (d: any): any => {
						var value = data.color;
						if (axisType.minValueScaleCero) {
							if (d !== undefined)
								if (d[axisType.valueName] < 0) {
									value = "red";
								}
						}
						return value;
					})
					.attr("r", that.showCircles ? that.radioCircle : 0)
					.attr("cx", (d: any, i: number) => {
						return that.axisBottom.scale(d[that.axisBottom.valueName]);
					})
					.attr("cy", (d: any, i: number) => {
						var value = axisType.scale(d[axisType.valueName]);
						if (axisType.minValueScaleCero) {
							if (d[axisType.valueName] < 0) {
								value = axisType.scale(0);
							}
						}
						return value;
					})
					.attr("stroke-width", that.showCircles ? that.strokeWidthCircle : 0);
			});
		circleTarget.exit().remove();
		circleTarget.enter()
			.append("circle")
			// .attr("fill", "white")//data.color)
			.attr("stroke", (d: any) => {
				var value = data.color;
				if (axisType.minValueScaleCero) {
					if (d[axisType.valueName] < 0) {
						value = "red"
					}
				}
				return value;
			})
			.attr("r", this.showCircles ? this.radioCircle : 0)
			.attr("cx", (d: any, i: number) => {
				return this.axisBottom.scale(d[this.axisBottom.valueName]);
			})
			.attr("cy", (d: any, i: number) => {
				var value = axisType.scale(d[axisType.valueName]);
				if (axisType.minValueScaleCero) {
					if (d[axisType.valueName] < 0) {
						value = axisType.scale(0);
					}
				}
				return value;
			})
			.attr("stroke-width", this.showCircles ? this.strokeWidthCircle : 0);
	}

	private drawLine(axisType: AxisConfig, data: Data, target: any): void {
		var that = this;
		var valueline = {} as any;
		valueline = line()
			.x((d: any) => { return this.axisBottom.scale(d[this.axisBottom.valueName]); })
			.y((d: any) => {
				var value = axisType.scale(d[axisType.valueName]);
				if (axisType.minValueScaleCero) {
					if (d[axisType.valueName] < 0) {
						value = axisType.scale(0);
					}
				}
				return value;
			})
			.curve(this.curveLine);
		var lineTarget = target
			.selectAll("g.line")
			.data([this.rejectNull(data.values, axisType.valueName)])
			.each(function (this: any) {
				select(this).select("path")
					.transition().duration(750)
					.attr("fill", "none")
					.attr("stroke", (d: any): any => data.color)
					.attr("stroke-width", that.strokeWidthLine)
					.attr("d", valueline);
			})

		lineTarget.exit().remove();
		lineTarget
			.enter()
			.append("g")
			.attr("class", "line")
			.append("path")
			.attr("fill", "none")
			.attr("stroke", data.color)
			.attr("stroke-width", this.strokeWidthLine)
			.attr("d", valueline);
	}

	private drawArea(axisType: AxisConfig, data: Data, target: any): void {
		if (this.showArea) {
			let Area = {} as any;
			Area = area()
				.x((d: any) => {
					return this.axisBottom.scale(d[this.axisBottom.valueName]);
				})
				.y0(this.height - this.margin.top - this.margin.bottom)
				.y1((d: any) => {
					var value = axisType.scale(d[axisType.valueName]);
					if (axisType.minValueScaleCero) {
						if (d[axisType.valueName] < 0) {
							value = axisType.scale(0);
						}
					}
					return value;
				})
				.curve(this.curveLine);
			//.interpolate(option.interpolate);
			var that = this;
			var areaTarget = target
				.selectAll("g.area")
				.data([this.rejectNull(data.values, axisType.valueName)])
				.each(function (this: any) {
					select(this).select("path")
						.transition().duration(750)
						.attr("d", Area)
						.attr("fill", (d: any): any => data.color)
						.attr("fill-opacity", that.areaOpacity);
				});
			areaTarget.exit().remove();
			areaTarget
				.enter()
				.datum(this.rejectNull(data.values, axisType.valueName))
				.append("g")
				.attr("class", "area")
				.append("path")
				.attr("d", Area)
				.attr("fill", data.color)
				.attr("fill-opacity", this.areaOpacity);
		}
	}

	public empty(): this {
		this.data = new Array();
		this.dates = new Array();
		this.render();
		var html = this.emptyGraphicContainer
			.append("div")
			.attr("class", "empty-graphic")
			.append("span")
			.html(this.emptyMessage);

		var h = html._groups[0][0].getBoundingClientRect().height;
		var w = html._groups[0][0].getBoundingClientRect().width;

		let top = (this.height / 2) - (h / 2);
		let left = (this.width / 2) - w / 2;
		html.style("top", `${top}px`).style("left", `${left}px`)
		return this;
	}

	public getData(): any {
		return this.data;
	}

	public getTarget(): any {
		return this.target;
	}

	private initContainers(): void {
		this.svgContainer = this.target.append("svg")
			.attr("width", this.width)
			.attr("height", this.height);

		this.axisBottomContainer = this.svgContainer
			.append("g")
			.attr("class", "axis-bottom-container");
		this.axisLeftContainer = this.svgContainer
			.append("g")
			.attr("class", "axis-left-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.axisRightContainer = this.svgContainer
			.append("g")
			.attr("class", "axis-right-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.entitiesTarget = this.svgContainer
			.append("g")
			.attr("class", "entities-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");
		this.tooltipContainer = this.target.append("div")
			.attr("class", "tooltip-container")
			.style("margin-left", this.margin.left + "px")
			.style("width", (this.width - this.margin.left - this.margin.right) + "px")
			.append("div")
			.attr("class", "tooltip-chart")
			.style("opacity", "0");
		this.hoverContainer = this.target.append("div")
			.attr("class", "hover-container")
			.style("left", this.margin.left + "px")
			.style("top", this.margin.top + "px")
			.append("div");
		this.emptyGraphicContainer = this.target.append("div").attr("class", "empty-graphic-container");
		this.axisRightConfig.containerElement = this.axisRightContainer;
		this.axisLeft.containerElement = this.axisLeftContainer;
	}

	private renderAxisLeft(): void {
		var min = this.setMin(this.axisLeft);
		min = this.axisLeft.minValueScaleCero ? 0 : min;
		var max = this.setMax(this.axisLeft);
		max = this.axisLeft.addMaxValueScalePercentage > 0 ? ((this.axisLeft.addMaxValueScalePercentage * max) / 100) + max : max;
		this.axisLeft.containerElement = this.axisLeftContainer;
		this.axisLeft.setScale((scaleObject: AxisConfig) => {
			scaleObject.scale = scaleLinear()
				.domain(this.axisLeft.reverseYdomain ? [max, min] : [min, max])
				.range([this.height - this.margin.top - this.margin.bottom, 0]);
		});
		if (!this.axisLeft.show) return;
		var axis = {} as any;
		axis = axisLeft(this.axisLeft.scale)
			.tickSize(this.axisLeft.showLineTicks ? -(this.width - (this.margin.left + this.margin.right)) : this.axisLeft.minTickSize)
			.tickPadding(this.axisLeft.tickPadding)
			.tickFormat(this.axisLeft.tickFormat == undefined ? (d, i) => {
				return d;
			} : this.axisLeft.tickFormat)
			.ticks(this.axisLeft.ticks);

		this.axisLeft.containerElement.transition().duration(750).call(axis);

		this.axisLeft.containerElement.selectAll("path")
			.attr("stroke", () => { return this.axisLeft.showLineTicks ? "none" : "#e8e8e8" })
			.attr("stroke-width", () => { return this.axisLeft.showLineTicks ? "0" : "2px" });

		if (this.axisLeft.showMinAndMaxTicks) {
			var groupText = this.axisLeft.containerElement.selectAll(".tick text");
			if (groupText._groups.length) {
				groupText.each(function (this: any, d: any, i: number) {
					if (i != 0 && i != groupText._groups[0].length - 1) {
						select(this).text("");
					}
				});
			}
		}
	}

	private renderAxisRight(): void {
		var min = this.setMin(this.axisRightConfig);
		min = this.axisRightConfig.minValueScaleCero ? 0 : min;
		var max = this.setMax(this.axisRightConfig);
		max = this.axisRightConfig.addMaxValueScalePercentage > 0 ? ((this.axisRightConfig.addMaxValueScalePercentage * max) / 100) + max : max;

		this.axisRightConfig.setScale((scaleObject: AxisConfig) => {
			scaleObject.scale = scaleLinear()
				.domain(this.axisRightConfig.reverseYdomain ? [max, min] : [min, max])
				.range([this.height - this.margin.top - this.margin.bottom, 0]);
		});
		if (!this.axisRightConfig.show) return;
		var axis = {} as any;
		axis = axisRight(this.axisRightConfig.scale)
			.tickSize(this.axisRightConfig.showLineTicks ? this.width - (this.margin.left + this.margin.right) : this.axisRightConfig.minTickSize)
			.tickPadding(this.axisRightConfig.tickPadding)
			.tickFormat(this.axisRightConfig.tickFormat === undefined ? (d, i) => {
				return d;
			} : this.axisRightConfig.tickFormat)
			.ticks(this.axisRightConfig.ticks);

		this.axisRightConfig.containerElement.transition().duration(750).call(axis);

		this.axisRightConfig.containerElement.selectAll("path")
			.attr("stroke", () => { return this.axisRightConfig.showLineTicks ? "none" : "#e8e8e8" })
			.attr("stroke-width", () => { return this.axisRightConfig.showLineTicks ? "0" : "2px" });

		if (this.axisRightConfig.showLineTicks == false) {
			this.axisRightConfig.containerElement.attr("transform", `translate(${[this.width - (this.margin.right), this.margin.top]})`)
		}

		if (this.axisRightConfig.showMinAndMaxTicks) {
			var groupText = this.axisRightConfig.containerElement.selectAll(".tick text");
			if (groupText._groups.length) {
				groupText.each(function (this: any, d: any, i: number) {
					if (i != 0 && i != groupText._groups[0].length - 1) {
						select(this).text("");
					}
				});
			}
		}
	}

	public renderAxisBottom(renderAfter: Function): void {
		let tickSize = this.axisBottom.showLineTicks ? -(this.height - (this.margin.top + this.margin.bottom)) : 0;

		this.axisBottom.containerElement = this.axisBottomContainer.attr("transform", "translate(" + [this.margin.left, (this.height - (this.margin.bottom))] + ")");

		this.axisBottom.setScale((scaleObject: AxisConfig) => {
			scaleObject.scale = scaleTime()
				.domain([this.dates[0], this.dates[this.dates.length - 1]])
				.range([0, this.width - (this.margin.left + this.margin.right)])
		});
		if (!this.axisBottom.show) {
			renderAfter();
			return;
		};

		let axis = this.setAxisBottomDefault(tickSize);

		this.axisBottom.containerElement.transition().duration(750).call(axis)
			.on("end",
				() => {
					this.renderAxisIfOutRangeofWithContainer();
					renderAfter();
				});

		if (this.axisBottom.showLineTicks == false)
			this.axisBottom.containerElement.selectAll(".tick line").attr("y2", this.axisBottom.minTickSize);
	}

	public checkTextWidthOfAxisBottom(): [boolean, number] {
		let tickPadding = this.axisBottom.tickPadding;
		let totalTicks = new Array();
		this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
			totalTicks.push(select(this).node().getBBox().width);
		});
		var sum = 0;

		if (totalTicks.length)
			sum = totalTicks.reduce((a, b) => (a + b) + tickPadding);
		var maxValue = max(totalTicks) + 10;
		if (sum > this.width) {
			return [true, maxValue];
		} else {
			return [false, maxValue];
		}
	}

	public resize() {
		window.addEventListener("resize", () => {
			clearTimeout(this.id);
			if (this.data.length > 0)
				this.id = setTimeout(() => {
					this.render();
				}, 1000);
		});
	};

	/**
	 * If it is true create two scale left and right, if the valueName are differents in the AxisConfig and
	 * take just two first values of the Data array
	 * @param ifTwoYscales by default false
	 */
	public setIfTwoYscales(ifTwoYscales: boolean): this {
		this.ifTwoYscales = ifTwoYscales;
		return this;
	}

	public setShowAreaLine(showArea: boolean): this {
		this.showArea = showArea;
		return this;
	}

	public setAxisBottomDefault(tickSize: number): Axis<AxisDomain> {
		return axisBottom(this.axisBottom.scale)
			.tickFormat(this.axisBottom.tickFormat === undefined ? (d: any, i) => {
				return this.parseTime(d);
			} : this.axisBottom.tickFormat)
			.tickPadding(this.axisBottom.tickPadding)
			.tickSize(tickSize)
			.ticks(this.dates.length);
	}

	/**
	 * d3 curve interpolation - e.m. curveLinear , curveCatmullRom etc.
	 * @param callback Function(chart, curveLinesOptions))
	 */
	public setCurveLine(callback: Function): this {
		callback.call(undefined, this, this.curveLinesOptions);
		return this;
	}

	/**
	 * set configuration to axis left
	 * @param callback Function(chart, axisLeftConfig), 
	 */
	public setAxisLeft(callback: Function): this {
		callback.call(this, this.axisLeft);
		return this;
	}

	/**
	 * set configuration to axis right
	 * @param callback Function(chart, axisRightConfig), 
	 */
	public setAxisRight(callback: Function): this {
		callback.call(this, this.axisRightConfig);
		return this;
	}

	/**
	 * set configuration to axis bottom
	 * @param callback Function(chart, axisBottomConfig), 
	 */
	public setAxisBottom(callback: Function): this {
		callback.call(this, this.axisBottom);
		return this;
	}

	/**
	 * @param dataset {data:[any],date:[new Dates]}
	 */
	public setData(dataset: GraphicData): this {
		if (dataset.data) {
			this.data = dataset.data.map((dd: any, i: number) => {
				dd.color = dd.color === undefined ? this.scaleColor(i.toString()) : dd.color;
				dd.values = dd.values.sort((a: any, b: any) => { return a[this.axisBottom.valueName].getTime() - b[this.axisBottom.valueName].getTime() });
				dd.values = dd.values.map((v: any) => {
					v.color = dd.color;
					return v;
				});
				return dd;
			});
		}
		this.dates = dataset.dates.sort((a: Date, b: Date) => { return a.getTime() - b.getTime() });
		return this;
	}

	public setTooltipCallback(callback: Function): this {
		this.tooltipCallback = callback;
		return this;
	}

	public setRadioCircle(radioCircle: number): this {
		this.radioCircle = radioCircle;
		return this;
	}

	public setStrokeWidthCircle(strokeWidthCircle: number): this {
		this.strokeWidthCircle = strokeWidthCircle;
		return this;
	}

	public setStrokeWidthLine(strokeWidthLine: number): this {
		this.strokeWidthLine = strokeWidthLine;
		return this;
	}

	public setWidth(width: number): this {
		this.width = width;
		return this;
	}

	public setHeight(height: number): this {
		this.height = height;
		return this;
	}

	public setMargin(margin: { top: number, bottom: number, left: number, right: number }): this {
		this.margin = { ...this.margin, ...margin };
		return this;
	}

	public setResponsive(responsive: boolean): this {
		this.responsive = responsive;
		return this;
	}

	public setAreaOpacity(areaOpacity: string): this {
		this.areaOpacity = areaOpacity;
		return this;
	}

	private setMax(axis: AxisConfig): number {
		let filteredData = new Array<Data>();
		if (this.ifTwoYscales) {
			filteredData = this.data.filter((value: Data) => value.axisType == axis.getType());
		} else {
			filteredData = this.data;
		}
		let unioned = unionBy(filteredData.map((v, i) => v.values));
		let maxValue = max(unioned, (d: any, i: any): number | undefined => {
			if (d[axis.valueName] !== undefined) {
				return d[axis.valueName];
			}
		});
		if (maxValue == undefined) {
			maxValue = 0;
		}
		return maxValue;
	}

	private setMin(axis: AxisConfig): number {
		let filteredData = new Array<Data>();
		if (this.ifTwoYscales) {
			filteredData = this.data.filter((value: Data) => value.axisType == axis.getType());
		} else {
			filteredData = this.data;
		}
		let unioned = unionBy(filteredData.map((v, i) => v.values));
		let minValue = min(unioned, (d: any, i: any): number | undefined => {
			if (d[axis.valueName] !== undefined) {
				return d[axis.valueName];
			}
		});
		if (minValue == undefined) {
			minValue = 0;
		}
		return minValue;
	}

	public setShowCircles(showCircles: boolean): this {
		this.showCircles = showCircles;
		return this;
	}

	public setEmptyMessage(emptyMessage: string): this {
		this.emptyMessage = emptyMessage
		return this;
	}

	private showCircle(d: any) {
		var that = this;
		this.svgContainer.selectAll("circle").each(function (this: any, c: any) {
			if (d.toDateString() == c[that.axisBottom.valueName].toDateString()) {
				select(this)
					.attr("r", that.radioCircle + 1)
					.attr("fill", select(this).attr("stroke"))
					.attr("stroke-width", 0)
					.attr("class", "overed-circle");
			}
		});
	}

	private hideCircle(d: any) {
		var that = this;
		this.svgContainer.selectAll("circle").each(function (this: any, c: any) {
			if (d.toDateString() == c[that.axisBottom.valueName].toDateString()) {
				select(this)
					.attr("r", that.showCircles ? that.radioCircle : 0)
					.attr("stroke-width", that.showCircles ? that.strokeWidthCircle : 0)
					.attr("class", "");
			}
		});
	}

	private renderAxisIfOutRangeofWithContainer() {
		let renderaxis = this.checkTextWidthOfAxisBottom();
		if (renderaxis[0]) {
			this.margin = { ...this.margin, bottom: renderaxis[1] };
			let tickSize = this.axisBottom.showLineTicks ? -(this.height - (this.margin.top + this.margin.bottom)) : 0;
			let axis = this.setAxisBottomDefault(tickSize);
			this.axisBottom.containerElement.call(axis);
			this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
				select(this).transition().duration(350).attr("transform", "translate(-25,15)rotate(-60)");
			});
		} else {
			this.axisBottom.containerElement.selectAll(".tick text").each(function (this: any, d: any) {
				select(this).transition().duration(350).attr("transform", "translate(0)rotate(0)");
			});
		}
	}

	private tooltipHtml(filterValues: any, date: Date): string {
		var ul = `<h6>${this.parseTime(date)}</h6><ul>`;
		filterValues.forEach((d: any, index: number) => {
			var circle = `<div class="_circle_index" style="border-color:${d.color}"></div>`;
			ul += `<li>${circle} ${d.name} : <b>${d[this.axisRightConfig.valueName] != undefined ? d[this.axisRightConfig.valueName] : d[this.axisLeft.valueName]}</b></li>`;
		});
		return `${ul} </ul>`;
	}

	public render(): void {
		if (this.responsive) {
			this.width = this.target.style("width") === "0px" ? this.width : parseInt(this.target.style("width").replace("px", "")) || this.width;
		}
		this.updateContainer();
		//clear message
		this.target.select(".empty-graphic-container").html("");
		//hide because I don't want to see the tooltip before renderizar the graphic
		this.target.select(".hover-container").style("display", "none");
		//check width of axis ticks
		this.renderAxisBottom(() => {
			this.renderAxisLeft();
			if (this.ifTwoYscales)
				this.renderAxisRight();
			this.draw(this.ifTwoYscales);
		});

	}

	private updateContainer(): void {
		this.target
			.select("svg")
			.attr("width", this.width);

		this.target
			.select(".tooltip-container")
			.style("margin-left", this.margin.left + "px")
			.style("width", (this.width - this.margin.left - this.margin.right) + "px");

		this.target
			.select(".axis-left-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");

		this.target
			.select(".entities-container")
			.attr("transform", "translate(" + [this.margin.left, this.margin.top] + ")");

		this.hoverContainer.select(".hover-container")
			.style("left", this.margin.left + "px")
			.style("top", this.margin.top + "px");
	}

	private rejectNull(data: any, valueName: string) {
		return data.filter((element: any) =>
			element[valueName] !== null
		);
	}
}

export function run(targetId: string) {
	return new lineChartd3(targetId);
}