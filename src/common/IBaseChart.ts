import { GraphicData } from "../utils/utilities";
export interface IBaseChart{
	empty(): this;
	render(): void;
	resize(): void;
	setData(dataset: GraphicData): this;
	setTooltipCallback(callback: Function): this;
	setResponsive(responsive: boolean): this;
	setEmptyMessage(emptyMessage: string): this;
}