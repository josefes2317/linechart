import { IAxisConfig } from "./IAxisConfig";
export class AxisConfig implements IAxisConfig {
	private type: AxisTypes;
	public containerElement: any;
	public tickFormat: any;
	public showMinAndMaxTicks: boolean = false;
	public reverseYdomain: boolean = false;
	public show: boolean = true;
	public ticks: number = 5;
	public tickPadding: number = 5;
	public showLineTicks: boolean = true;
	public addMaxValueScalePercentage: number = 0;
	public minValueScaleCero: boolean = false;;
	public scale: any;
	public valueName: string;
	public minTickSize: number = 0;

	/**
	 * 
	 * @param type It acepts just 3 types 'bottom', 'left' or 'right' options by default 'bottom'
	 * @param valueName It is value key by default is 'Value'
	 */
	constructor(type: AxisTypes = AxisTypes.bottom, valueName: string = "Value") {
		this.valueName = valueName;
		this.type = type;
	}
	getType(): AxisTypes {
		return this.type;
	}

	setScale(callback: Function): this {
		callback.call(undefined, this);
		return this;
	}
}

export enum AxisTypes {
	bottom = "bottom",
	left = "left",
	right = "right"
}