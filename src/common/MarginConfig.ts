export class MarginConfig {
    public left: number = 0;
    public right: number = 0;
    public bottom: number = 0;
    public top: number = 0;

    constructor(left: number, top: number, right: number, bottom: number) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }
}