import { MarginConfig } from "./MarginConfig";
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from "d3-scale-chromatic";
import { IBaseChart } from "./IBaseChart";
import { GraphicData } from "../utils/utilities";
export abstract class BaseChart implements IBaseChart {
	target: any;
	svgContainer: any;
	tooltipContainer: any;
	hoverContainer: any;
	emptyGraphicContainer: any;
	emptyMessage: string = "Empty";
	tooltipCallback: any;
	margin: MarginConfig = new MarginConfig(40, 15, 40, 40);
	width: number = 300;
	height: number = 260;
	responsive: boolean = true;
	scaleColor = scaleOrdinal(schemeCategory10);
	abstract empty(): this;
	abstract render(): void;
	abstract resize(): void;
	abstract setData(dataset: GraphicData): this;
	abstract setTooltipCallback(callback: Function): this;
	abstract setResponsive(responsive: boolean): this;
	abstract setEmptyMessage(emptyMessage: string): this;
}
