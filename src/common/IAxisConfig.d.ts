export interface IAxisConfig {
    containerElement: any;
    tickFormat: any;
    showMinAndMaxTicks: boolean;
    reverseYdomain: boolean;
    show: boolean;
    ticks: number;
    showLineTicks: boolean;
    tickPadding: number;
    scale: any;
    valueName: string;
    addMaxValueScalePercentage: number;
    minValueScaleCero: boolean;
    /**
     * this options is active when 'showLineTicks' option is equal to false
     */
    minTickSize: number;
    setScale(callback: Function): void;
}
