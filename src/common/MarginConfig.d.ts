export declare class MarginConfig {
    left: number;
    right: number;
    bottom: number;
    top: number;
    constructor(left: number, top: number, right: number, bottom: number);
}
