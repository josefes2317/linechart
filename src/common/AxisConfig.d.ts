import { IAxisConfig } from "./IAxisConfig";
export declare class AxisConfig implements IAxisConfig {
    private type;
    containerElement: any;
    tickFormat: any;
    showMinAndMaxTicks: boolean;
    reverseYdomain: boolean;
    show: boolean;
    ticks: number;
    tickPadding: number;
    showLineTicks: boolean;
    addMaxValueScalePercentage: number;
    minValueScaleCero: boolean;
    scale: any;
    valueName: string;
    minTickSize: number;
    /**
     *
     * @param type It acepts just 3 types 'bottom', 'left' or 'right' options by default 'bottom'
     * @param valueName It is value key by default is 'Value'
     */
    constructor(type?: AxisTypes, valueName?: string);
    getType(): AxisTypes;
    setScale(callback: Function): this;
}
export declare enum AxisTypes {
    bottom = "bottom",
    left = "left",
    right = "right",
}
