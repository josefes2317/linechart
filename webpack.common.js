var path = require('path');

module.exports = {
	entry: {
		"lineChartd3": "./src/lineChart/index.ts",
		"clusteredBarChartd3": "./src/barChart/index.ts"
	},
	output: {
		filename: '[name].js',
		library: '[name]',
		libraryTarget: 'umd',
		path: __dirname + "/lib"
	},
	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [".ts", ".tsx", ".js", ".json"]
	},
	module: {
		rules: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
			{ test: /\.tsx?$/, loader: "ts-loader" }
		]
	},
	plugins: [],
	externals: {
		// "react": "React",
		// "react-dom": "ReactDOM",
		// "rxjs": "rxjs"
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				// commons: {
				// 	test: /[\\/]node_modules[\\/](i18next|i18next-xhr-backend|react-i18next|react-loading-skeleton)[\\/]/,
				// 	name: 'core.common',
				// 	chunks: 'initial'
				// }
			}
		}
	}
};
