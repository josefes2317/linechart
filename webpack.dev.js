var Merge = require("webpack-merge");
var CommonConfig = require("./webpack.common.js");

module.exports = Merge(CommonConfig, {
	mode: "development",
	// Enable sourcemaps for debugging webpack's output.
	devtool: "source-map",
	plugins: [
		// new CleanWebpackPlugin(),
	],
	watch: true,
	watchOptions: {
		aggregateTimeout: 300,
		poll: 500
	}
});