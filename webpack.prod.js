var Merge = require("webpack-merge");
var common = require('./webpack.common');

module.exports = Merge(common, {
	mode: "production",
	devtool: false,
	plugins: []
});
